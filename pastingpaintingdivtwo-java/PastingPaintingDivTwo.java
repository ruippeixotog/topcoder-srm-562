public class PastingPaintingDivTwo {

	public char[][] toBoard(String[] clipboard) {
		char[][] board = new char[clipboard.length][];
		for(int i = 0; i < clipboard.length; i++) {
			board[i] = clipboard[i].toCharArray();
		}
		return board;
	}
	
	public int sim(char[][] clipboard, int T) {
		for(int k = 1; k < T; k++) {
			for(int i = k; i < clipboard.length; i++) {
				if(clipboard[i - 1][k - 1] == 'B') {
					clipboard[i][k] = 'B';
				}
			}
			for(int j = k; j < clipboard[0].length; j++) {
				if(clipboard[k - 1][j - 1] == 'B') {
					clipboard[k][j] = 'B';
				}
			}
		}
		int count = 0;
		for(int i = 0; i < clipboard.length; i++) {
			for(int j = 0; j < clipboard[0].length; j++) {
				if(clipboard[i][j] == 'B') {
					count++;
				}
			}
		}
		return count;
	}
	
	public int diagIdx(int i, int j, int width) {
		return i - j + width - 1;
	}

	public int diagLength(int i, int h, int w) {
		return i < w ? i : w - (i - w);
	}

	public long countColors(String[] clipboard, int T) {
		int height = clipboard.length;
		int width = clipboard[0].length();

		boolean[] diagonal = new boolean[width + height - 1];
		for (int i = 0; i < clipboard.length; i++) {
			for (int j = 0; j < clipboard[i].length(); j++) {
				if (clipboard[i].charAt(j) == 'B')
					diagonal[diagIdx(i, j, width)] = true;
			}
		}
		int count = 0;
		for(int i = 0; i < diagonal.length; i++) {
			if(diagonal[i])
				count++;
		}
		
		int totalBlacks = 0;
		for (int i = 0; i < clipboard.length; i++) {
			for (int j = 0; j < clipboard[i].length(); j++) {
				if (clipboard[i].charAt(j) == 'B')
					totalBlacks++;
			}
		}
		
		char[][] board = toBoard(clipboard);
		int realSim = sim(board, Math.min(width, height));
		for(int i = 0; i < height; i++) {
			board[i][0] = diagonal[diagIdx(i, 0, width)] ? 'B' : '.';
		}
		for(int j = 0; j < height; j++) {
			board[0][j] = diagonal[diagIdx(0, j, width)] ? 'B' : '.';
		}
		int errorSim = sim(board, Math.min(width, height));
		
		if(T < Math.min(width, height)) {
			return realSim;
		}
		return count * (long) (T - 1) + totalBlacks + (realSim - errorSim);
	}

}
