
public class CucumberMarket {

	public String check(int[] price, int budget, int k) {
		return choose(price, budget, k, 0, 0) ? "YES" : "NO";
	}
	
	boolean choose(int[] price, int budget, int k, int m, int currPrice) {
		if(k == 0) {
			return currPrice <= budget;
		}
		for(int i = m; i < price.length; i++) {
			if(!choose(price, budget, k - 1, i + 1, currPrice + price[i]))
				return false;
		}
		return true;
	}

}
